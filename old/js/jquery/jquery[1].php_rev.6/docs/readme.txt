= jQuery PHP

    Library for work with jQuery framework from server-side.

== Requirements

    * PHP 5.2.0 or higher (with JSON extension)
    * jQuery 1.2.1 or higher

== Documentation

    Please see the jQuery PHP  documentation on site: http://jquery.hohli.com

== Author

    name: Anton Shevchuk
    homepage: http://anton.shevchuk.name
    
== History

    0.6 - 2008.07.28
        + example "ajax loading image" with small image "ajax-loader.gif"
        + example "error handler"
        - remove method "jQuery" (cause an E_STRICT error)
        ! very important, now "jQuery::jQuery('#..')" is not working, use short call "jQuery('#..')"

    0.5 - 2008.07.07
        + history :)
        + alias in PHP - now aviable function jQuery (see example)
        * jQuery 1.2.6 in example
        * remove call '$' function - only full name - jQuery
        
    0.4 - 2008.04.15
        # not remember
        
    0.3 - 2008.01.09
        * hotfix
        
    0.2 - 2008.01.04
        # first public revision
        
        
legend
 "+" added new feature
 "-" remove some stuff
 "*" fix bug or some changes
 "#" comment
 "!" very important comment