User-agent: *

Disallow: /application/
Disallow: /application/*

Disallow: /data/
Disallow: /data/*

Disallow: /library/
Disallow: /library/*

Disallow: /logs/
Disallow: /logs/*

Disallow: /css/
Disallow: /css/*

Disallow: /js/
Disallow: /js/*

Disallow: /images/
Disallow: /images/*
