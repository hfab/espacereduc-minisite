<?php

//definition de l'encodage
 //iconv_set_encoding("internal_encoding", "UTF-8");  
// iconv_set_encoding("output_encoding", "ISO-8859-1");

 iconv_set_encoding("output_encoding", "UTF-8");
 iconv_set_encoding("input_encoding", "UTF-8");


//error_reporting(E_ALL|E_STRICT);
error_reporting(E_ALL ^ E_NOTICE);//rapporter toutes les erreurs sauf enotice
ini_set('display_errors', 1);


ini_set('SMTP', '127.0.0.1');
ini_set('smtp_port', '25');


date_default_timezone_set('Europe/Paris');



//constantes recette
define('SITEURL', 'http://www.plandefou.com/');
define('CSSDIR', '/var/www/plandefou//public/css/');
define('IMAGESDIR', '/var/www/plandefou/public/images/');
define('FONTDIR', '/var/www/plandefou//public/font/arial.ttf');

define('IMGDIR', '/var/www/plandefou/public/images/captcha/');
define('CAPTCHADIR', '/images/captcha/');

define('IMAGESFORMDIR', '/images/');

 
define('BASE_PATH',realpath(dirname(__FILE__) . './'));
define('APPLICATION_ENVIRONMENT', 'developpement');



// mise en place des répertoires et chargement des classes
 set_include_path(".:/opt/ZendFramework/current/library". PATH_SEPARATOR . get_include_path());
/* chemin vers les la librairie zend et vers les models /objet*/  
set_include_path(BASE_PATH . '../library'
				. PATH_SEPARATOR . '../application/models/' 
                           . PATH_SEPARATOR . '../application/admin/models'
				. PATH_SEPARATOR . get_include_path());

/* l'appel automatique des classes*/
require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);

//$autoloader->registerNamespace('My_');


Zend_Session::start();


//appel du bootstrap
require BASE_PATH .'../application/bootstrap.php';

bootstrap();


